# LaTeX Preview Element

This is a preview input element for use in ESG (Editor Screen Generator)

## Installation

- This package requires NodeJS, version 6 or above.
- Only Google Chrome browser is supported

```Shell
npm install eslint bower -g
npm install
```

## Development

```Shell
npm start
```

## Build

```Shell
npm run build
```

## Test

```Shell
npm install web-component-tester -g
npm test
```