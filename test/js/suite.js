const equation = 'y_0 = 4 + 5x^2';

describe('initialization', () => {
    it('can render equation', done => {
        const el = document.querySelector('latex-preview');

        el.setAttribute('value', equation);
        expect(el.getAttribute('value')).to.equal(equation);


        setTimeout(done, 1000);
    });
});
