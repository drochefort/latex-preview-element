/* global Polymer */
const MATHJAX_CDN_URL = '//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML&delayStartupUntil=configure';
const EDITOR_URL = 'http://visualmatheditor.equatheque.net/VisualMathEditor.html?codeType=Latex&equation=';

class LatexPreviewElement extends HTMLElement {
    /**
     * This method acts like the constructor in Polymer context
     */
    beforeRegister() {
        this.is = 'latex-preview';

        this.properties = {
            value: {
                type: String,
                value: '\\int_0^{\\infty} x^2 + 4',
                notify: true,
                observer: 'onValueChange',
            },
            editorLink: {
                type: String,
                value: '',
            },
        };
    }

    /**
     * Element is created
     */
    created() {
        // Initialize MathJax
        if (!self.mathJax) {
            const script = document.createElement('script');
            script.setAttribute('src', MATHJAX_CDN_URL);
            script.onload = this.configureMathJax.bind(this);
            self.document.body.appendChild(script);
        }
    }

    /**
     * Element is ready
     */
    ready() {
        // Add internal event listeners
        this.$.input.addEventListener('change', this.onEquationChange.bind(this));

        this.updateEditorLink();
    }

    /**
     * Set the element's value every time equation is changed
     */
    onEquationChange() {
        this.value = this.$.input.value;
    }

    /**
     * Re-render the preview every time the value is changed
     */
    onValueChange(value) {
        if (value && self.MathJax) {
            this.toggleClass('hidden', true, this.$.preview);

            this.$.preview.innerHTML = '$$' + this.value + '$$';

            this.updateEditorLink();

            if (self.MathJax.isReady) {
                self.MathJax.Hub.Typeset(this.$.preview);
            }
        }
    }

    updateEditorLink() {
        this.editorLink = EDITOR_URL + encodeURIComponent(this.value);
    }

    /**
     * Configure MathJax and register custom message hooks
     * TODO: put in separate math library 
     */
    configureMathJax() {
        // Register MathJax hooks
        if (self.MathJax) {
            self.MathJax.Hub.Register.StartupHook('End', this.onRenderComplete.bind(this));
            self.MathJax.Hub.Register.MessageHook('End Process', this.onRenderComplete.bind(this));

            self.MathJax.Hub.Config({
                TeX: {
                    extensions: ['enclose.js', 'cancel.js'],
                },
                messageStyle: 'none',
            });

            self.MathJax.Hub.Configured();
        }
    }

    /**
     * Show the content once Mathjax finished its rendering
     * TODO: put in separate math library 
     */
    onRenderComplete() {
        this.highlightDigits();
        this.toggleClass('hidden', false, this.$.preview);
    }

    /**
     * Apply standard transformation to MathJax generated HTML to highlight digits
     * TODO: put in separate math library 
     */
    highlightDigits() {
        var numberNodes = document.querySelectorAll('.MathJax_Display span');

        Array.prototype.forEach.call(numberNodes, function (node) {
            if (node.style.color && node.style.color !== 'black') {
                node.style.backgroundColor = node.style.color;
                node.style.color = '';
            }
        });
    }
}

// Initialize element with Polymer
Polymer(LatexPreviewElement);