const fs = require('fs');
const Vulcanize = require('vulcanize');

const vulcan = new Vulcanize({
    inlineScripts: true,
    inlineCss: true,
});

function exists(dir) {
    try {
        const stat = fs.statSync(dir);
        return stat.isDirectory();
    } catch (e) {
        return false;
    }
}

vulcan.process('src/latex-preview.html', function (err, html) {
    if (!exists('build')) {
        fs.mkdirSync('build');
    }

    fs.writeFileSync('build/latex-preview.html', html);
});