// require the module as normal
const browserSync = require('browser-sync');

// Start the server
browserSync({
    server: './',
    files: [
        'src/*.html',
        'src/js/*.js',
        'src/css/*.css',
    ],
});